# Python scripts

This is a collection of my python code.

__podcasts/Feed.py__: Utilities to create and host feeds using Amazon S3.  

_makeFeedFromS3Bucket_: Makes an XML feed from mp3 files stored in an Amazon bucket. Feed is stored on S3 and a feed URL for your podcast player is returned.

_getUrlsFromMultipleOpmls_: Reads URLs from one or more OPML files.  Can be used to obtain URLs to feed into other functions.

_combineMultipleFeeds_: Reads multiple feed URLs and generates a single feed file that is hosted on S3. Non-essential tags are removed so if a feed doesn't work in your podcast player, try specifying a single URL to "rip" a feed.

_makeFeedFromListOfUrls_: Makes a feed from a list of mp3 URLs.  This is used to generate feeds for content that doesn't have a feed - like open coursework lectures.

See __podcasts/examples.py__ for examples.

