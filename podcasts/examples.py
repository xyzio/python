#!/usr/bin/python

import os
from Feed import Feed

if __name__ == '__main__':

	# Set up s3 variables for boto3
	os.environ['AWS_ACCESS_KEY_ID'] = ''
	os.environ['AWS_SECRET_ACCESS_KEY'] = ''
	os.environ['AWS_DEFAULT_REGION'] = 'us-west-2'

	# Set up variables
	Feed.uploadBucket = 'upload_bucket_name'
	Feed.fileTypeFilter = 'mp3'
	# Pre-fix for s3 region 
	Feed.s3LinkRoot = 's3-us-west-2.amazonaws.com'

	feed = Feed()

	#Made feed from files in an s3 bucket
	feedUrl = feed.makeFeedFromS3Bucket('bucket',r'key_filter', title="Feed title", description = "Feed description", outputFileName='hom.xml')

	#Read URls out of an opml file
	opmls = [r'./sample_opml/opml1.opml']
	urls = Feed.getUrlsFromMultipleOpmls(opmls)

	#Combine multiple feeds into one file or re-rip a troublesome feed
	feedUrl = feed.combineMultipleFeeds(urls, "Combined feed list", 'A single feed combining all feeds', 'outputFileName.xml')

	#Create a feed out of a list of mp3 URLs
	feed = Feed()
	urls = [r'http://www.mfiles.co.uk/mp3-downloads/moonlight-movement1.mp3','http://www.mfiles.co.uk/mp3-downloads/Beethoven-Symphony5-1.mp3']
	feedUrl = feed.makeFeedFromListOfUrls(urls, outputFileName="FeedFromListOfUrls.xml", title="A sample list", link="http://xyzio.com", description = "This is a test")

