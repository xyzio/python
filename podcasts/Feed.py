#!/usr/bin/python

import boto3
import datetime
import uuid
import requests
from xml.dom import minidom
from natsort import natsorted, ns
import re

try:
	import xml.etree.cElementTree as etree
except ImportError:
	import xml.etree.ElementTree as etree

class FeedUtils:
	"""Misc utilities to help build a feed"""
	def __init__(self):
		pass

	@staticmethod
	def getHeaders(url):
		""""Get http headers for a URL"""
		data = {}
		try:
			response = requests.head(url)

			for header in response.headers:
				data[header] = response.headers[header]

		except:
			print 'Error during FeedUtils.getHeaders'

		return data

	@staticmethod
	def getEnclosureObj(url):
		"""Builds an Enclosure object given an URL"""
		headers = FeedUtils.getHeaders(url)

		contentType = ''
		contentUrl = url
		contentLength = ''

		# For each URL, build an Item
		# Build enclosure
		if 'Content-Type' in headers.keys():
			contentType = headers['Content-Type']
		if 'Content-Length' in headers.keys():
			contentLength = headers['Content-Length']

		enclosure = Enclosure(contentType, contentUrl, contentLength)

		return enclosure


class Feed:
	"""Holds information about a feed"""

	uploadBucket = ''
	fileTypeFilter = ''
	s3LinkRoot = ''

	def __init__(self):
		self.title = ''
		self.link = ''
		self.lastBuildDate = ''
		self.description = ''
		self.items = []
		pass

	@staticmethod
	def makePubDate(dateObj):
		"""
		Creates rss compatible string from datetime.datetime object
		:param dateObj: Datetime.datetime object
		:return: A rss compatible string
		:rtype: basestring
		"""
		dateStr = Feed.getDayFromNumber(dateObj.weekday()) + ', '
		dateStr += str(dateObj.day) + ' '
		dateStr += str(Feed.getMonthFromNumber(dateObj.month)) + ' '
		dateStr += str(dateObj.year) + ' '
		dateStr += ':'.join([str(x) for x in (dateObj.hour, dateObj.minute, dateObj.second)]) + ' '
		dateStr += '+0000'

		return dateStr

	@staticmethod
	def getDayFromNumber(dayNumber):
		"""Gets a numerical version of a 3-letter day"""
		dayNames = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
		return dayNames[dayNumber]

	@staticmethod
	def getMonthFromNumber(monthNumber):
		"""Gets a numerical version of a 3-letter month"""
		monthNames = ['', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
		return monthNames[monthNumber]

	@staticmethod
	def writeFileToS3(bucketName, data, fileName, contentType):
		"""Writes a data object to s3"""
		#Upload to s3

		s3 = boto3.resource('s3')
		s3.Bucket(bucketName).put_object(Key=fileName, Body=data, ACL='public-read', ContentType=contentType)

		s3 = boto3.client('s3')
		feedUrl = '{}/{}/{}'.format(s3.meta.endpoint_url, bucketName, fileName)

		return feedUrl


	@staticmethod
	def getUrlsFromBucket(bucketName, feedFolderName):
		"""Gets a list of URLs in bucket = bucketName and key_filter = feedFolderName"""
		urls = []
		s3 = boto3.resource('s3')
		# Get key
		for key in s3.Bucket(bucketName).objects.all():
			#print key.key
			if (key.key.endswith(Feed.fileTypeFilter)) and (feedFolderName in key.key):
				#Todo: Update URL here
				keyUrl = 'http://' + '/'.join([Feed.s3LinkRoot, bucketName, key.key])
				print keyUrl
				urls.append(keyUrl)

		#Naturally sort urls
		urls = natsorted(urls, alg=ns.IGNORECASE)

		return urls


	@staticmethod
	def getUrlsFromMultipleOpmls(opmls):
		"Reads URLs out an OPML file"
		output = set()

		for opml in opmls:
			e = etree.parse(opml).getroot()
			for elem in e.iterfind('body/outline'):
				#print elem.tag, elem.attrib
				url = elem.attrib['xmlUrl']
				#text = elem.attrib['text']
				#feedType = elem.attrib['type']
				#feeds.append(Feed(url, text, feedType))

				output.add(url)

		return list(output)



	def makeFeedFromListOfUrls(self, urls=[], outputFileName = 'FeedFromListOfUrls.xml', title = 'title', link = 'link', description = 'description'):
		"""Combines a list of mp3 URLs into a feed"""

		self.title = title
		self.link = link
		self.description = description

		self.populateItemsFromFileUrls(urls)

		xml = self.makeFeed()
		feedUrl = Feed.writeFileToS3(Feed.uploadBucket, xml, outputFileName, r'text/xml')

		return feedUrl



	def makeFeed(self):
		"""
		Creates a feed using the title, link, description, and items stored in this Feed instance.
		Returns a string containing the XML for the feed
		"""
		nsMap = {'xsi': 'http://www.w3.org/2001/XMLSchema-instance'}
		# location_attribute = '{%s}noNamespaceSchemaLocation' % nsMap['xsi']

		root = etree.Element('rss', {'version': '2.0'})
		channel = etree.SubElement(root, 'channel')
		title = etree.SubElement(channel, 'title')
		title.text = self.title

		link = etree.SubElement(channel, 'link')
		link.text = self.link

		# <lastBuildDate>Fri, 06 Jan 2017 15:58:27 +0000</lastBuildDate>
		today = datetime.datetime.today()
		lastBuildDate = etree.SubElement(channel, 'lastBuildDate')
		lastBuildDate.text = Feed.makePubDate(today)

		description = etree.SubElement(channel, 'description')
		description.text = self.description

		for item in self.items:
			nItem = etree.SubElement(channel, 'item')
			title = etree.SubElement(nItem, 'title')
			title.text = item.title

			nDescription = etree.SubElement(nItem, 'description')
			nDescription.text = item.description

			nEnclosure = etree.SubElement(nItem, 'enclosure', {'type': item.enclosure.type, 'url': item.enclosure.url,
															   'length': item.enclosure.length})

			nPubDate = etree.SubElement(nItem, 'pubDate')
			nPubDate.text = item.pubDate

			nGuid = etree.SubElement(nItem, 'guid', {'isPermaLink': 'false'})
			nGuid.text = item.guid

			nCategory = etree.SubElement(nItem, 'category')
			nCategory.text = item.category

			nLink = etree.SubElement(nItem, 'link')
			nLink.text = item.link

		xml = prettify(root)
		return xml

	def combineMultipleFeeds(self, listOfFeedsToCombine, title, description, outputFileName):
		"""Combine multiple feed URLs into one URL with all items from all feeds"""

		self.title = title
		#Todo: Update URL here
		self.link= r'http://'  + Feed.s3LinkRoot + r'/' + outputFileName
		self.description = description

		#For each feed
		items = []
		for url in listOfFeedsToCombine:
			#Get xml from URL
			userAgent ='Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'

			try:
				response = requests.get(url, headers={'User-Agent': userAgent })
			except:
				print 'Error: Unable to parse feed for url = ' + url

			#Create item object for each item
			e = etree.ElementTree(etree.fromstring(response.content)).getroot()
			itemTitle = itemDescription = itemPubDate = itemGuid = itemCategory = itemLink = itemEnclosure = ''	

			for elem in e.findall('channel/item'):
				for itemParams in elem.iter():
					if re.match(r'title$',itemParams.tag):
						itemTitle = itemParams.text
					
					if 'description' in itemParams.tag:
						itemDescription = itemParams.text

					if 'pubDate' in itemParams.tag:
						itemPubDate = itemParams.text

					if 'guid' in itemParams.tag:
						itemGuid = itemParams.text

					if 'category' in itemParams.tag:
						itemCategory = itemParams.text

					if 'link' in itemParams.tag:
						itemLink = itemParams.text

					if 'enclosure' in itemParams.tag:
						itemEnclosure = Enclosure(itemParams.attrib['type'], itemParams.attrib['url'], itemParams.attrib['length'])

				
				self.items.append(Item(itemTitle, itemDescription, itemPubDate, itemGuid, itemCategory, itemLink, itemEnclosure))

		#Create XML
		xml = self.makeFeed()

		#Upload XML to S3
		feedUrl = Feed.writeFileToS3(Feed.uploadBucket, xml, outputFileName, r'text/xml')
		return feedUrl

		pass


	def populateItemsFromFileUrls(self, fileUrls):
		"""Takes in a list of mp3 urls and outputs Item objects"""

		today = datetime.datetime.today()

		urlCounter = 0
		for url in fileUrls:
			enclosure = FeedUtils.getEnclosureObj(url)

			pubDate = Feed.makePubDate(today + datetime.timedelta(days=(-1 * urlCounter)))
			title = url.split('/')[-1]
			description = title
			link = '/'.join(url.split('/')[0:-2])
			self.items.append(Item(title, description, pubDate, str(uuid.uuid1()), 'category', link, enclosure))

			urlCounter += 1

		return

	def makeFeedFromS3Bucket(self, bucketName, feedFolderName, title = 'This is the title', link = 'This is the link', description = 'This is the description', outputFileName='output.xml'):
		"""Creates a feed with mp3s stored in an S3 bucket"""
		self.title = title
		self.link = link
		self.description = description

		urls = Feed.getUrlsFromBucket(bucketName, feedFolderName)

		self.populateItemsFromFileUrls(urls)

		#Make feed from title, description, link, and items
		xml =self.makeFeed()
		feedUrl = Feed.writeFileToS3(Feed.uploadBucket, xml, outputFileName, r'text/xml')

		#print xml
		print feedUrl
		pass


class Enclosure:
	"""Holds information about a feed Enclosure object"""
	def __init__(self, type, url, length):
		self.type = type
		self.url = url
		self.length = length

class Item:
	"""Holds information about an Item in feed"""
	def __init__(self, title, description, pubDate, guid, category, link, enclosure):
		self.title = title
		self.description = description
		self.pubDate = pubDate
		self.guid = guid
		self.category = category
		self.link = link
		self.enclosure = enclosure


def prettify(elem):
	"""Return a pretty-printed XML string for the Element.
	"""
	rough_string = etree.tostring(elem, encoding = 'utf-8')
	reparsed = minidom.parseString(rough_string)
	return reparsed.toprettyxml(indent='\t', encoding='utf-8')



